package services

import (
	"../models"
	"../pkg/commons/crypto"
	"../pkg/requests"
	"../pkg/requests/accounts"
	"../pkg/responses"
	"../pkg/responses/messages"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"regexp"
	"strconv"
)

// CreateUser godoc
// @Summary Tạo tài khoản
// @Accept  json
// @Produce  json
// @Tags accounts
// @Param body body accounts.Account true "model"
// @Success 200 {object} responses.SuccessRes
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Bad request"
// @Failure 404 {string} string "Not found"
// @Failure 500 {string} string "Internal server error"
// @Router /accounts/[post]
func CreateUser(c *gin.Context) {
	app := responses.Gin{C: c}
	user := models.User{}
	var err error
	if err = c.BindJSON(&user); err != nil {
		app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
		return
	}
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(user.Email) {
		app.Response(http.StatusBadRequest, messages.FORMAT_EMAIL_ERROR, nil)
		return
	}
	exist, err := models.IsUsernameExist(user.UserName)
	if err != nil {
		app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
		return
	}
	if !exist {
		exist, err = models.IsEmailExist(user.Email)
		if err != nil {
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
	}
	if exist {
		app.Response(http.StatusBadRequest, messages.DUPLICATE_USER, nil)
		return
	}
	pwd, err := crypto.HashPassword(user.Password)
	if err != nil {
		app.Response(http.StatusBadRequest, messages.INVALID_PARAMS, nil)
		return
	}
	user.Password = pwd
	res := models.Create(user)
	if !res {
		app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
		return
	}
	result := responses.SuccessRes{
		Data:    true,
		Message: messages.GetMessage(messages.CREATE_SUCCESS)}
	c.JSON(http.StatusOK, result)
	return
}

// ForgotPwd godoc
// @Summary Quên mật khẩu
// @Description ACB Test
// @Accept  json
// @Produce  json
// @Tags accounts
// @Param body body accounts.ForgotPwd true "model"
// @Success 200 {object} responses.SuccessRes
// @Failure 400 {string} string "Bad request"
// @Failure 500 {string} string "Internal server error"
// @Router /accounts/forgot-password/[post]
func ForgotPwd(c *gin.Context) {
	app := responses.Gin{C: c}
	obj := accounts.ForgotPwd{}
	var err error
	if err = c.BindJSON(&obj); err != nil {
		app.Response(http.StatusBadRequest, messages.INVALID_PARAMS, nil)
		return
	}
	user, exist := models.FindUserByEmail(obj.Email)
	if !exist {
		app.Response(http.StatusBadRequest, messages.INVALID_PARAMS, nil)
		return
	}
	if exist {
		err = models.UpdateForgotCode(user)
		if err != nil {
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
		result := responses.SuccessRes{
			Data:    true,
			Message: "Vui lòng kiểm tra hộp thư điện tử",
		}
		c.JSON(http.StatusOK, result)
		return
	}
}

// VerifyForgotPwdCode godoc
// @Summary Kiểm tra token forgotpassword
// @Description ACB Test
// @Accept  json
// @Produce  json
// @Tags accounts
// @Param token path string true "token"
// @Success 200 {object} responses.SuccessRes
// @Failure 400 {string} string "Bad request"
// @Failure 500 {string} string "Internal server error"
// @Router /accounts/verify-forgotpwd/{token}/[get]
func VerifyForgotPwdCode(c *gin.Context) {
	code := c.Param("token")
	res := models.VerifyForgotPwdCode(code)
	var result = responses.SuccessRes{
		Data:    res,
		Message: messages.GetMessage(messages.SUCCESS),
	}
	c.JSON(http.StatusOK, result)
	return
}

// UpdateForgotPwd godoc
// @Summary update quên mật khẩu
// @Description ACB Test
// @Accept  json
// @Produce  json
// @Tags accounts
// @Param model body accounts.UpdateForgotPwd true "model"
// @Success 200 {object} responses.SuccessRes
// @Failure 400 {string} string "Bad request"
// @Failure 500 {string} string "Internal server error"
// @Router /accounts/update-forgotpwd/[put]
func UpdateForgotPwd(c *gin.Context) {
	obj := accounts.UpdateForgotPwd{}
	app := responses.Gin{C: c}
	var err error
	if err = c.BindJSON(&obj); err != nil {
		app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
		return
	}
	res := models.UpdateForgotPwd(obj)
	if res.StatusCode != http.StatusOK {
		app.ResponseObj(res)
		return
	}
	c.JSON(http.StatusOK, res)
	return
}

// GetUsers godoc
// @Summary Danh sách tài khoản
// @Description ACB Test
// @Accept  json
// @Produce  json
// @Tags accounts
// @Param filter query string false "Filter"
// @Param pageIndex query int false "PageIndex"
// @Param limit query int false "Limit"
// @Success 200 {object} responses.SuccessRes
// @Failure 400 {string} string "Bad request"
// @Failure 500 {string} string "Internal server error"
// @Security ApiKeyAuth
// @Router /accounts/[get]
func GetUsers(c *gin.Context) {
	//claims, _ := util.AuthInstance.Authenticate(token)
	//userId, _ := claims["id"].(uint)

	filter := c.Query("filter")
	pageIndex, err := strconv.Atoi(c.Query("pageIndex"))
	limit, err := strconv.Atoi(c.Query("limit"))
	obj := requests.QueryArgs{
		PageIndex: pageIndex,
		Limit:     limit,
		Filter:    filter,
	}
	app := responses.Gin{C: c}
	res, err := models.GetUsers(obj)
	if err == gorm.ErrRecordNotFound {
		app.Response(http.StatusBadRequest, messages.INVALID_PARAMS, nil)
		return
	}
	if res.TotalItems == 0 {
		app.Response(http.StatusNotFound, messages.DATA_NOTFOUND, nil)
		return
	}
	var result = responses.SuccessRes{
		Data:    res,
		Message: messages.GetMessage(messages.SUCCESS),
	}
	c.JSON(http.StatusOK, result)
	return
}

// UpdatePwd godoc
// @Summary Cap nhat mat khau
// @Description ACB Test
// @Accept  json
// @Produce  json
// @Tags accounts
// @Param model body accounts.ChangePassword true "password"
// @Success 200 {object} responses.SuccessRes
// @Failure 400 {string} string "Bad request"
// @Failure 500 {string} string "Internal server error"
// @Security ApiKeyAuth
// @Router /accounts/me/[patch]
func UpdatePwd(c *gin.Context) {
	param := c.Param("password")
	id, exist := c.Get("currentUserId")
	app := responses.Gin{C: c}
	if !exist {
		app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
		return
	}
	res := models.UpdatePwd(id.(int), param)
	if res.StatusCode != http.StatusOK {
		app.ResponseObj(res)
		return
	}
	c.JSON(http.StatusOK, res)
	return
}
