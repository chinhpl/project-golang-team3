package services

import (
	"../core/authJwt"
	"../core/author"
	"../core/utils"
	"../models"
	"../pkg/commons/crypto"
	"../pkg/requests/authentications"
	"../pkg/responses"
	"../pkg/responses/messages"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// Credential godoc
// @Summary Đăng nhập
// @Accept  json
// @Produce  json
// @Tags authentication
// @Param body body authentications.Crendetial true "Model"
// @Success 200 {object} responses.LoginRes
// @Failure 400 {string} string "Bad request"
// @Failure 404 {string} string "Not found"
// @Failure 500 {string} string "Internal server error"
// @Router /authentications/credentials [post]
func Credential(c *gin.Context)  {
	now := time.Now()
	app := responses.Gin{C: c}
	obj:= authentications.Crendetial{}
	var err error
	if err = c.BindJSON(&obj); err != nil {
		fmt.Println("err", err)
		app.Response(http.StatusBadRequest, messages.INVALID_PARAMS, nil)
		return
	}
	user, exist := models.FindUserByUserName(obj.UserName)
	if !exist{
		app.Response(http.StatusBadRequest, messages.LOGIN_MESSAGE, nil)
		return
	}
	if exist{
		err = models.CheckAndUpdatePendingTime(user)
		if err != nil{
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
		if(user.SpendingTime != nil && user.SpendingTime.After(now)){
			app.Response(http.StatusForbidden, messages.SPENDING_LOGIN, nil)
			return
		}
		comparePwd:= crypto.CheckPasswordHash(obj.Password, user.Password)
		if !comparePwd{
			_, errUpLock:= models.UpdateCountLock(user)
			if errUpLock != nil{
				app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
				return
			}
			app.Response(http.StatusBadRequest, messages.LOGIN_MESSAGE, nil)
			return
		}
		err = models.ClearCountLock(user)
		if err != nil{
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
		resUser := author.PayloadToken{
			UserName: user.UserName,
			ID: user.ID,
			FullName: user.FullName,
		}

		payload, err := author.SetPayloadToken(resUser)
		if err != nil {
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
		accessToken, err:= utils.AuthInstance.GenerateToken(payload, authJwt.ExpiredTokenDuration)
		if err != nil {
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
		payload = map[string]interface{}{
			"id":     resUser.ID,
			"unique_name": resUser.ID,
		}
		refreshToken, err := utils.RefreshInstance.GenerateToken(payload, authJwt.ExpiredRefreshTokenDuration)
		if err != nil {
			app.Response(http.StatusInternalServerError, messages.SYSTEM_ERROR, nil)
			return
		}
		result := responses.LoginRes{
			AccessToken:accessToken,
			RefeshToken:refreshToken,
		}
		c.JSON(http.StatusOK, result)
	}
}