package responses

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"../responses/messages"
)

type Response struct {
	StatusCode int    `json:"statusCode,omitempty"`
	Error      string `json:"error,omitempty"`
	Message    string `json:"message,omitempty"`
}

type Gin struct {
	C *gin.Context
}

func (g *Gin) Response(httpCode int, errorCode int, data interface{})  {
	g.C.JSON(httpCode,Response{
		StatusCode:httpCode,
		Error:http.StatusText(httpCode),
		Message: messages.GetMessage(errorCode),
	})
	return
}

func (g *Gin) BadRequest(err error) {
	if err != nil{
		g.Response(http.StatusBadRequest, messages.INVALID_PARAMS, nil)
		return
	}
}

func (g *Gin) ResponseObj(res Response)  {
	g.C.JSON(res.StatusCode,res)
	return
}