package responses

type ResultPaging struct {
	Items interface{} `json:"items"`
	TotalItems int `json:"total_items"`
} 
