package responses


type SuccessRes struct {
	Message string
	Data interface{}
}