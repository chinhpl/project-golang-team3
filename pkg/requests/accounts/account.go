package accounts

type Account struct {
	Email             string     `json:"email"`
	UserName          string     `json:"user_name"`
	FullName          string     `json:"full_name"`
	Password          string     `json:"password"`
}
