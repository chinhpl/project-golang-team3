package requests

type QueryArgs struct {
	Filter string `json:"filter"`
	PageIndex int `json:"page_index"`
	Limit int `json:"limit"`
} 