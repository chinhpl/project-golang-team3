package author

import (
	"../utils"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func Author() gin.HandlerFunc  {
	return func(c *gin.Context) {
		tokenStr := c.GetHeader("Authorization")
		if tokenStr == ""{
			c.JSON(http.StatusUnauthorized, nil)
			c.Abort()
			return
		}
		if strings.Contains(tokenStr, "Bearer "){
			splitStr := strings.Split(tokenStr, " ")
			if len(splitStr) != 2 {
				c.JSON(http.StatusBadRequest, nil)
				c.Abort()
				return
			}
			tokenStr = splitStr[1]
		}
		claims,err := utils.AuthInstance.Authenticate(tokenStr)
		if err != nil {
			// Signature doesn't match
			switch err.(*jwt.ValidationError).Errors {
			case jwt.ValidationErrorExpired, jwt.ValidationErrorSignatureInvalid:
				c.JSON(http.StatusUnauthorized, nil)
				c.Abort()
				return
			default:
				c.JSON(http.StatusBadRequest, nil)
				c.Abort()
				return
			}
		}
		iAreaId, ok := claims["id"].(int)
		if !ok{
			idunit, ok := claims["id"].(uint)
			if !ok{
				panic(ok)
			}
			iAreaId = int(idunit)
		}
		c.Set("currentUserId",iAreaId)
		c.Next()
	}
}


func SetPayloadToken(user PayloadToken) (map[string]interface{}, error) {

	return map[string]interface{}{
		"id":                    user.ID,
		"unique_name":           user.UserName,
	}, nil
}

type PayloadToken struct {
	UserName string
	FullName string
	ID int
}