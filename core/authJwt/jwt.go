package authJwt

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const secondsDelay = 3 * 60

type Claims map[string]interface{}

const (
	ExpiredTokenDuration        = 4 * time.Hour
	ExpiredRefreshTokenDuration = 24 * time.Hour
)


type JWTAuth struct {
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
}

func InitJWTAuth(filePathPubKey string, filePathPrivKey string) *JWTAuth {
	authInstance := &JWTAuth{
		privateKey: ReadPrivateKeyFromPEM(filePathPrivKey),
		publicKey:  ReadPublicKeyFromPEM(filePathPubKey),
	}
	return authInstance
}

func ReadPublicKeyFromPEM(filename string) *rsa.PublicKey {
	key, readErr := ioutil.ReadFile(filename)
	if readErr != nil {
		panic("failed to load public key file")
	}
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		fmt.Errorf("error parsing RSA public key: %v\n", err)
		panic(err)
	}
	return publicKey
}

func ReadPrivateKeyFromPEM(filename string) *rsa.PrivateKey {
	file, err := os.Open(filename)
	key := make([]byte, 99999)
	_, readErr := file.Read(key)
	// key, readErr := ioutil.ReadFile(filename)
	if readErr != nil {
		panic("failed to load private key file")
	}
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		fmt.Errorf("error parsing RSA private key: %v\n", err)
		panic(err)
	}
	return privateKey
}

func (auth *JWTAuth) GenerateToken(claims Claims, expiredDuration time.Duration) (string, error) {
	_claims := jwt.MapClaims(claims)
	now := time.Now()
	if expiredDuration != 0 {
		_claims["exp"] = now.Add(expiredDuration).Unix()
	}
	_claims["nbf"] = now.Unix() - secondsDelay
	_claims["iat"] = now.Unix() - secondsDelay
	tokenString, err := jwt.NewWithClaims(jwt.SigningMethodRS256, _claims).SignedString(auth.privateKey)
	if err != nil {
		panic(err)
		return "", err
	}
	return tokenString, nil
}

func (auth *JWTAuth) Authenticate(tokenPart string) (map[string]interface{}, error) {
	claims := jwt.MapClaims{} // Initialize a new instance of `Claims`

	// Parse JWT string and store results in `claims`.
	// Passing the secret key in this method. This method will return an error
	token, err := jwt.ParseWithClaims(tokenPart, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return auth.publicKey, nil
	})
	if token != nil && token.Valid {
		payloads := token.Claims.(jwt.MapClaims)
		payloads["id"] = AsUInt(payloads["id"])
		payloads["unique_name"] = payloads["unique_name"]
		return payloads, nil
	}
	return nil, err
}

func AsUInt(input interface{}) uint {
	var results uint
	if reflect.ValueOf(input).Kind() == reflect.String {
		results1, err := strconv.ParseUint(input.(string), 10, 32)
		if err != nil {
			panic(fmt.Sprintf("Error not convert type %v to %v", reflect.String, reflect.Uint))
		} else {
			results = uint(results1)
		}
	} else if reflect.ValueOf(input).Kind() == reflect.Uint {
		results = uint(input.(uint))
	} else {
		results = uint(input.(float64))
	}
	return results
}
