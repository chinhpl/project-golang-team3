package utils

import (
	"../../settings"
	"../authJwt"
)

var AuthInstance *authJwt.JWTAuth
var RefreshInstance *authJwt.JWTAuth

func Setup() {
	AuthInstance = authJwt.InitJWTAuth(settings.AppSetting.PublicKeyPath, settings.AppSetting.PrivateKeyPath)
	RefreshInstance = authJwt.InitJWTAuth(settings.AppSetting.PublicKeyRefreshPath, settings.AppSetting.PrivateKeyRefreshPath)
}
