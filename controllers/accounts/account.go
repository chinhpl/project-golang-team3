package accounts

import (
	"../../core/author"
	"../../services"
	"github.com/gin-gonic/gin"
)

func RouterAccount(g *gin.RouterGroup) *gin.RouterGroup {
	public := g.Group("accounts")

	public.POST("", services.CreateUser)
	public.POST("forgot-password", services.ForgotPwd)
	public.PUT("update-forgotpwd", services.UpdateForgotPwd)
	public.GET("verify-forgotpwd/:token", services.VerifyForgotPwdCode)
	public.Use(author.Author())
	{
		public.GET("", services.GetUsers)
		public.PATCH("me", services.UpdatePwd)
	}

	return public
}