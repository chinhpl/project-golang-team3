package controllers

import (
	"./accounts"
	"./auth"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	version1 := router.Group("/api/v1")
	{
		version1.GET("healthcheck",HealthCheck)
		accounts.RouterAccount(version1)
		auth.RouterAuth(version1)
	}
	return router
}

// HealthCheck godoc
// @Accept  json
// @Produce  json
// @Success 200 {object} string "Healthy"
// @Router /healthcheck/[get]
func HealthCheck(c *gin.Context)  {
	c.JSON(200, "Healthy")
}
